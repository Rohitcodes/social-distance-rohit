package org.altbeacon.beaconreference;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

public class SplashScreen extends Activity {
    String url1="http://34.123.218.195";
    public static final String MyPREFERENCES = "User_Credentials" ;
    public static final String Password = "password";
    public static final String Phone = "phone";
    public static final String status = "login_status";
    public static final String  register_token = "register_token";
    SharedPreferences sharedpreferences;
    ImageView logoiv;
    Intent i;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        logoiv = (ImageView) findViewById(R.id.logoimg);
        Animation myanim = AnimationUtils.loadAnimation(this, R.anim.transiotion);
        logoiv.startAnimation(myanim);
        if(!DetectConnection.checkInternetConnection(this))
            i=new Intent(SplashScreen.this,NoInternet.class);
        else
         i=new Intent(SplashScreen.this,MonitoringActivity.class);
        Thread timer=new Thread()
        {
            public void run()
            {
                try
                {
                    sleep(3000);
                }catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                finally {
                    startActivity(i);
                }

            }

        };
        timer.start();

    }
}