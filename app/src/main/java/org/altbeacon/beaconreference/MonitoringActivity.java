package org.altbeacon.beaconreference;
import  android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.PersistableBundle;
import android.os.RemoteException;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AlertDialogLayout;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;

import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.Region;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.concurrent.Delayed;

import static org.altbeacon.beaconreference.Download_File.url1;

/**
 *
 * @author dyoung
 * @author Matt Tyler
 */
@SuppressWarnings("deprecation")
public class MonitoringActivity extends Activity {
	protected static final String TAG = "MonitoringActivity";
	private static final int PERMISSION_REQUEST_FINE_LOCATION = 1;
	private static final int PERMISSION_REQUEST_BACKGROUND_LOCATION = 2;
	//private static String url="http://development.anzensafetyapp.com/login";
	private BeaconManager beaconManager = BeaconManager.getInstanceForApplication(this);
	BroadcastReceiver RbroadcastReceiver;
	private static final int PERMISSION_STORAGE_CODE = 1000;
	Handler handler;
	//	String url;
	WebView Wv;
	ProgressBar progressBar;
	SwipeRefreshLayout swipeRefreshLayout;
	TextView tv1;
	long time1;
	String s;
	SharedPreferences sharedpreferences;
	public static final String MyPREFERENCES = "User_Credentials";
	public static final String Password = "password";
	public static final String Phone = "phone";
	public static final String status = "login_status";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_monitoring);
		verifyBluetooth();
		Wv = findViewById(R.id.webView);
//		Intent i=getIntent();
//		url = i.getStringExtra("url");
		sharedpreferences = getSharedPreferences("User_Credentials", 0);
		progressBar = findViewById(R.id.p_id);
		swipeRefreshLayout = findViewById(R.id.swipe);
		JSCOde js = new JSCOde(this);
		//tv1=(TextView)findViewById(R.id.token12);
		Wv.addJavascriptInterface(js, "bstart");
		Wv.addJavascriptInterface(js, "bstop");
		Wv.addJavascriptInterface(js, "bsend");
		Wv.addJavascriptInterface(new Download_File(this), "Download");
		Wv.getSettings().setLoadWithOverviewMode(true);
		Wv.getSettings().setUseWideViewPort(true);
		Wv.getSettings().setSupportZoom(true);
		Wv.getSettings().setBuiltInZoomControls(true);
		Wv.getSettings().setDisplayZoomControls(false);
		Wv.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		Wv.setScrollbarFadingEnabled(false);
		Wv.getSettings().setJavaScriptEnabled(true);
		Wv.addJavascriptInterface(new Logincredentials(this), "Android");
		Wv.setWebChromeClient(new WebChromeClient());
		Wv.setWebViewClient(new WebViewClient());
		Wv.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				//Toast.makeText(getApplicationContext(), "In ShouldOvverideURL", Toast.LENGTH_SHORT).show();
				return true;
			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);
				progressBar.setVisibility(view.VISIBLE);
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				Wv.setVisibility(View.VISIBLE);
				progressBar.setVisibility(view.GONE);
			}

			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				if(errorCode==6)
					Wv.loadUrl("file:///android_asset/Errored.html");
				Toast.makeText(getApplicationContext(),"Server Not Available"+errorCode,Toast.LENGTH_SHORT).show();
			}

		});
		// Load the webpage
		Wv.loadUrl(getString(R.string.mainurl));
		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				swipeRefreshLayout.setRefreshing(true);
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						swipeRefreshLayout.setRefreshing(false);
						Wv.loadUrl(getString(R.string.mainurl));
					}
				}, 1000);
			}
		});

		swipeRefreshLayout.setColorSchemeColors(
				getResources().getColor(android.R.color.holo_blue_light),
				getResources().getColor(android.R.color.holo_orange_dark),
				getResources().getColor(android.R.color.holo_green_dark),
				getResources().getColor(android.R.color.holo_red_dark)
		);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
					== PackageManager.PERMISSION_GRANTED) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
					if (this.checkSelfPermission(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
							!= PackageManager.PERMISSION_GRANTED) {
						if (!this.shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_BACKGROUND_LOCATION)) {
							final AlertDialog.Builder builder = new AlertDialog.Builder(this);
							builder.setTitle("This app needs background location access");
							builder.setMessage("Please grant location access so this app can detect beacons in the background.");
							builder.setPositiveButton(android.R.string.ok, null);
							builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

								@TargetApi(23)
								@Override
								public void onDismiss(DialogInterface dialog) {
									requestPermissions(new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION},
											PERMISSION_REQUEST_BACKGROUND_LOCATION);
								}

							});
							builder.show();
						} else {
							final AlertDialog.Builder builder = new AlertDialog.Builder(this);
							builder.setTitle("Functionality limited");
							builder.setMessage("Since background location access has not been granted, this app will not be able to discover beacons in the background.  Please go to Settings -> Applications -> Permissions and grant background location access to this app.");
							builder.setPositiveButton(android.R.string.ok, null);
							builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

								@Override
								public void onDismiss(DialogInterface dialog) {
								}

							});
							builder.show();
						}
					}
				}
			} else {
				if (!this.shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
					requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
									Manifest.permission.ACCESS_BACKGROUND_LOCATION},
							PERMISSION_REQUEST_FINE_LOCATION);
				} else {
					final AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setTitle("Functionality limited");
					builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons.  Please go to Settings -> Applications -> Permissions and grant location access to this app.");
					builder.setPositiveButton(android.R.string.ok, null);
					builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

						@Override
						public void onDismiss(DialogInterface dialog) {
						}

					});
					builder.show();
				}

			}

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
				if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
						PackageManager.PERMISSION_DENIED) {
					String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
					requestPermissions(permissions, PERMISSION_STORAGE_CODE);

				}
			}
		}

		FirebaseInstanceId.getInstance().getInstanceId()
				.addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
					@Override
					public void onComplete(@NonNull Task<InstanceIdResult> task) {
						if (!task.isSuccessful()) {
							Log.w(TAG, "getInstanceId failed", task.getException());
							return;
						}
						// Get new Instance ID token
						String token = task.getResult().getToken();
						String verify_phone, verify_password = null;
						// Log and toast
						//	Log.d(TAG, msg);
						Toast.makeText(MonitoringActivity.this, "In FirebaseID MonitoringActivity"+token, Toast.LENGTH_SHORT).show();

						if (sharedpreferences.contains(Phone)) {
							verify_phone = sharedpreferences.getString(Phone, "");
							verify_password = sharedpreferences.getString(Password, "");
							String add_token = token;
							//tv1.setText(add_token);
							Wv.loadUrl(getString(R.string.mainurl));//+ "/autologin?phone="
							//+ verify_phone
							//	+ "&password=" + verify_password);
							verify_add_token(add_token, verify_phone, verify_password);
						}
						//		Toast.makeText(MonitoringActivity.this, "In FirebaseID MonitoringActivity"+token+"Phone No"+verify_phone+""+"Password"+verify_password+"Token"+add_token, Toast.LENGTH_SHORT).show();
					}
				});


	}

	@Override
	public void onRequestPermissionsResult(int requestCode,
										   String permissions[], int[] grantResults) {
		switch (requestCode) {
			case PERMISSION_REQUEST_FINE_LOCATION: {
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					Log.d(TAG, "fine location permission granted");
				} else {
					final AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setTitle("Functionality limited");
					builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons.");
					builder.setPositiveButton(android.R.string.ok, null);
					builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

						@Override
						public void onDismiss(DialogInterface dialog) {
						}

					});
					builder.show();
				}
				return;
			}
			case PERMISSION_REQUEST_BACKGROUND_LOCATION: {
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					Log.d(TAG, "background location permission granted");
				} else {
					final AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setTitle("Functionality limited");
					builder.setMessage("Since background location access has not been granted, this app will not be able to discover beacons when in the background.");
					builder.setPositiveButton(android.R.string.ok, null);
					builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface dialog) {
						}

					});
					builder.show();
				}
				return;
			}
		}
	}

	public void onRangingClicked() {
		Intent myIntent = new Intent(this, RangingActivity.class);
		myIntent.putExtra("String1","2");
		this.startActivity(myIntent);
	}
	public void onEnableClicked() {
		BeaconReferenceApplication application = ((BeaconReferenceApplication) this.getApplicationContext());
			application.enableMonitoring();
			}

	public void verify_add_token(final String token, String verify_phone , String verify_password) {
		RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
		//Toast.makeText(getApplicationContext(),"In Verify Add Tocken",Toast.LENGTH_LONG).show();
		JSONObject object = new JSONObject();
		try {
			//input your API parameters
			object.put("token" , token  );
			object.put("verify_phone", verify_phone );
			object.put("verify_password", verify_password );

		} catch (JSONException e) {
			e.printStackTrace();
		}
		// Enter the correct url for your api service site
		String url = getString(R.string.mainurl)+ "/register_token" ;
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, object,
				new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						String output = "";
						try {
							output = response.get("response").toString();
							if (output.matches("OK")){

							}

						} catch (JSONException e) {
							e.printStackTrace();
						}
						//resultTextView.setText("String Response : "+ output)
					}
				}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				//Toast.makeText(getApplicationContext(), "Error getting response"+error+""+token, Toast.LENGTH_SHORT).show();

			}
		});
		requestQueue.add(jsonObjectRequest);
	}



	@Override
    public void onResume() {
         BeaconReferenceApplication application = ((BeaconReferenceApplication) this.getApplicationContext());
        	application.setMonitoringActivity(this);
		//updateLog(application.getLog());
			super.onResume();
	}


	@Override
	public void onBackPressed() {
		Wv.reload();
		if(Wv.canGoBack()) {
			Wv.goBack();
		} else {
			super.onBackPressed();
		}
	}

	@Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(RbroadcastReceiver);
        ((BeaconReferenceApplication) this.getApplicationContext()).setMonitoringActivity(null);

	}

	private void verifyBluetooth() {
		try {
			if (!BeaconManager.getInstanceForApplication(this).checkAvailability()) {
				final AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("Bluetooth not enabled");
				builder.setMessage("Please enable bluetooth in settings and restart this application.");
				builder.setPositiveButton(android.R.string.ok, null);
				builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialog) {
						//finish();
			            //System.exit(0);
					}
				});
				builder.show();
			}
		}
		catch (RuntimeException e) {
			final AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Bluetooth LE not available");
			builder.setMessage("Sorry, this device does not support Bluetooth LE.");
			builder.setPositiveButton(android.R.string.ok, null);
			builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					//finish();
		            //System.exit(0);
				}

			});
			builder.show();

		}

	}

    public void updateLog(final String log) {
	/*	TextView textView = (TextView) MonitoringActivity.this
				.findViewById(R.id.monitoringText);
		textView.setText(log);
	*/
	}


	public void postData() {
		//Toast.makeText(getApplicationContext(),"Post Data",Toast.LENGTH_LONG).show();
		final String url = "http://192.168.1.92/ROHIT";
		JSONObject jsonParam = null;
		Calendar c = Calendar.getInstance();
		//SimpleDateFormat sdf=new SimpleDateFormat("H:M:S");
		//String date=sdf.format(c.getTime());
		Date ctime = Calendar.getInstance().getTime();
		RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
		final List<JSONObject> stringList = new ArrayList<>();

		final JSONArray jsonArray = new JSONArray();
		SharedPreferences sp = getSharedPreferences("Login", 0);
		SharedPreferences.Editor Ed = sp.edit();
		// jsonArray.put(jsonParam1);
		for (int i = 0; i < AdapterBeacon.allBeacons.size(); i++) {
		//	Toast.makeText(getApplicationContext(),"for Loop",Toast.LENGTH_LONG).show();
			jsonParam = new JSONObject();
			try {
				//input your API parameters
				jsonParam.put("Name", AdapterBeacon.allBeacons.get(i).getId1().toString().substring(19));
				jsonParam.put("ID1", AdapterBeacon.allBeacons.get(i).getId2().toString());
				jsonParam.put("Distance", AdapterBeacon.allBeacons.get(i).getDistance());
				jsonParam.put("Power", AdapterBeacon.allBeacons.get(i).getRssi());
				 } catch (JSONException e) {
				Toast.makeText(getApplicationContext(),"catch-->"+jsonParam,Toast.LENGTH_LONG).show();

			}
			jsonArray.put(jsonParam);
//			Toast.makeText(getApplicationContext(),"JARRAY"+jsonArray,Toast.LENGTH_LONG).show();
			Ed.putString("Unm", "Rohit Mali");
			Ed.putString("Psw", "Password");
			Ed.putString("Time", "" + ctime.toString().substring(0, 16));
			Ed.putString("Role", "Operator");
			Ed.putString("Data",""+jsonArray);
			Ed.commit();
		}
		// Enter the correct url for your api service site
		//Toast.makeText(getApplicationContext(),"Details1:-"+sp.getString("Data",""),Toast.LENGTH_LONG).show();
		final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST, url, jsonArray,
				new Response.Listener<JSONArray>() {
					@Override
					public void onResponse(JSONArray response) {
						// Toast.makeText(getApplicationContext(), "" + response + "\n" + jsonArray, Toast.LENGTH_LONG).show();
					}
				}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {
				Toast.makeText(getApplicationContext(), "" + volleyError + "\n" + jsonArray, Toast.LENGTH_LONG).show();
			}
		}) {

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> headers = new HashMap<String, String>();
				// Add headers
				return headers;
			}

			//Important part to convert response to JSON Array Again
			@Override
			protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
				String responseString;
				JSONArray array = new JSONArray();
				if (response != null) {

					try {
						responseString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
						JSONObject obj = new JSONObject(responseString);
						(array).put(obj);
					} catch (Exception ex) {
					}
				}
				//return array;
				return Response.success(array, HttpHeaderParser.parseCacheHeaders(response));
			}
		};
		requestQueue.add(jsonArrayRequest);
	//	Toast.makeText(getApplicationContext(),"Queues"+AdapterBeacon.allBeacons.size(),Toast.LENGTH_LONG).show();
		JSONObject jdata = new JSONObject();
		SharedPreferences sp1 = this.getSharedPreferences("Login", 0);
		JSONObject object = new JSONObject();
		final TreeMap<String, String> postParam = new TreeMap<String, String>();

		for(int i=0;i<AdapterBeacon.allBeacons.size();i++) {
			//JSONArray JSA=new JSONArray();
			postParam.put("1.UserName:-", sp1.getString("Unm", null));
			postParam.put("2.Password:-", sp1.getString("Psw", null));
			postParam.put("3.TimeStamp:-", sp1.getString("Time", null));
			postParam.put("4.Role:-", sp1.getString("Role", null));
			postParam.put("5.Data:-", sp1.getString("Data", null));
		}
	//	   Toast.makeText(getApplicationContext(),"SharedPreferences"+sp1.getString("Unm", null),Toast.LENGTH_LONG).show();

		final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(postParam),
				new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						String output = "";
						try {
							output = response.get("response").toString();
							Toast.makeText(getApplicationContext(), "String Response : " + output, Toast.LENGTH_LONG).show();
						} catch (JSONException e) {
							e.printStackTrace();
						}
						}
				},  new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				s=""+postParam;
				Toast.makeText(getApplicationContext(),"Beacons Size"+AdapterBeacon.allBeacons.size()+ "Error getting response"+error.toString()+"PostParams\n"+postParam, Toast.LENGTH_LONG).show();
			}
		});
		requestQueue.add(jsonObjectRequest);
		Toast.makeText(getApplicationContext(), "Total Beacons Detected---> " +AdapterBeacon.allBeacons.size(), Toast.LENGTH_LONG).show();
	}

	public class JSCOde {
		Context context;

		public JSCOde(Context context) {
			this.context = context;
		}
		@JavascriptInterface
		public void beaconStart(String toast) {
			onEnableClicked();
			onRangingClicked();
		}

		@JavascriptInterface
		public void beaconStop(String toast1)
		{
			Toast.makeText(getApplicationContext(),"Ranging Stopped\n",Toast.LENGTH_SHORT).show();
			postData();
			try {
				beaconManager.startMonitoringBeaconsInRegion(new Region("B9407F30-F5F8-466E-AFF9-25556B57FE6D", null, null, null));
				beaconManager.stopRangingBeaconsInRegion(new Region("B9407F30-F5F8-466E-AFF9-25556B57FE6D", null, null, null));
				handler.removeCallbacks(null);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		@JavascriptInterface
		public void sendbeaconData(String toast1) {
			handler=new Handler();
			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(),"Runnning",Toast.LENGTH_LONG).show();
					postData();			//your code
					handler.postDelayed(this,120000);
				}
			},120000);
		}
	}
}




