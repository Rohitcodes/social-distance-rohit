package org.altbeacon.beaconreference;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static org.altbeacon.beaconreference.AdapterBeacon.allBeacons;

public class RangingActivity extends Activity implements BeaconConsumer {
    protected static final String TAG = "RangingActivity";
    private BeaconManager beaconManager = BeaconManager.getInstanceForApplication(this);
    private ListView listview;
    Toolbar toolbar;
    private Button stp, sbmt;
    private ArrayList<Beacon> beaconList;
    private AdapterBeacon bAdapter;
    private ImageView ivNavigate;
    private ImageButton ibNavigate;
    private ImageButton ibstscan;
    //public static String s="";
    //Handler handler = null;
    //Runnable runnable;
    //RequestQueue requestQueue;
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranging);
        beaconList = new ArrayList<Beacon>();
        this.bAdapter = new AdapterBeacon(this, beaconList);
        ivNavigate=(ImageView)findViewById(R.id.ivNav);
        ibNavigate=(ImageButton)findViewById(R.id.iback);
        //ibstscan=(ImageButton)findViewById(R.id.stop);
        listview = (ListView) findViewById(R.id.listView1);
        listview.setAdapter(bAdapter);
        long starttime=0;
        long millis = System.currentTimeMillis() - starttime;
        int seconds = (int) (millis / 1000);
        int minutes = seconds / 60;
        seconds     = seconds % 60;
        //RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        ibNavigate.setOnClickListener(
                new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent i=new Intent(RangingActivity.this,MonitoringActivity.class);
               startActivity(i);
            }
       });


          /*stp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stp.getText().equals("Stop")) {
                    try {
                        stp.setText("Start");
                        beaconManager.stopRangingBeaconsInRegion(new Region("B9407F30-F5F8-466E-AFF9-25556B57FE6D", null, null, null));

                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                } else if (stp.getText().equals("Start")) {

                    stp.setText("Stop");
                    onBeaconServiceConnect();
                }

            }
        });
*/
    }
/*
    public void timeData() {
        final SharedPreferences prefs = getSharedPreferences("Data", 0);
        final int[] count = {0};
        if (prefs.getLong("timer", 1000) <= 0) {
            Toast.makeText(getApplicationContext(), "make some time", Toast.LENGTH_SHORT).show();
        } else {
            handler = new Handler();
            runnable = new Runnable() {
                public void run() {
                    try {
                        beaconManager.startRangingBeaconsInRegion(new Region("B9407F30-F5F8-466E-AFF9-25556B57FE6D", null, null, null));
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    //postData();
                    Toast.makeText(getApplicationContext(), "running" + count[0], Toast.LENGTH_SHORT).show();
                    count[0]++;
                    handler.postDelayed(this, prefs.getLong("timer", 0));
                }
            };
            runnable.run();
            handler.removeCallbacks(runnable);
        }
    }
*/



/*
    private void SubmitData() {
        for (int i = 0; i < AdapterBeacon.allBeacons.size(); i++) {
            JSONObject PostData = new JSONObject();
            try {
                PostData.put("Name", AdapterBeacon.allBeacons.get(i).getId1().toString().substring(19));
                PostData.put("ID1", AdapterBeacon.allBeacons.get(i).getId2().toString());
                PostData.put("Distance", AdapterBeacon.allBeacons.get(i).getDistance());
                PostData.put("Power", AdapterBeacon.allBeacons.get(i).getRssi());
                //   Toast.makeText(getApplicationContext(),"In Submit Data"+AdapterBeacon.name1.substring(19),Toast.LENGTH_SHORT).show();
                AsyncTask.Status s = new SendBeaconDetails().execute("http://192.168.1.152/POST_DATA", PostData.toString()).getStatus();
                Toast.makeText(getApplicationContext(), "status-->" + s.toString() + "\n" + PostData.toString(), Toast.LENGTH_LONG).show();
            } catch (JSONException JSE) {
                JSE.printStackTrace();
            }
        }
    }
*/




    @Override
    protected void onDestroy()
    {
    //   handler.removeCallbacks(runnable);
        super.onDestroy();
        beaconManager.unbind(this);
    }

    @Override 
    protected void onPause() {
        super.onPause();
        beaconManager.unbind(this);
    }

    @Override 
    protected void onResume() {
        super.onResume();

        beaconManager.bind(this);
    }

    @Override
    public void onBeaconServiceConnect() {
        RangeNotifier rangeNotifier = new RangeNotifier() {
           @Override
           public void didRangeBeaconsInRegion(final Collection<Beacon> beacons, Region region) {
               if (beacons.size() > 0) {
                        if (listview.getAdapter() == null) {
                            listview.setAdapter(bAdapter);
                           } else {
                          ((AdapterBeacon)listview.getAdapter()).copyBeacons(beacons);
                        }
          }
       }
      };
        try {
            beaconManager.startRangingBeaconsInRegion(new Region("B9407F30-F5F8-466E-AFF9-25556B57FE6D", null, null, null));
            beaconManager.addRangeNotifier(rangeNotifier);
            beaconManager.startRangingBeaconsInRegion(new Region("B9407F30-F5F8-466E-AFF9-25556B57FE6D", null, null, null));
            beaconManager.addRangeNotifier(rangeNotifier);
        } catch (RemoteException e) {   }

       }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //MenuInflater menuInflater=getMenuInflater();
        getMenuInflater().inflate(R.menu.beacon_menu,menu);
        return true;
    }
    /*
        public void Handleit()
        {
            final SharedPreferences spd=getSharedPreferences("Data",0);
            long t=spd.getLong("timer",0);
            if(t==0 || t<0) {
                t=1000;
                Toast.makeText(getApplicationContext(), "Mention Some Time" + t, Toast.LENGTH_SHORT).show();
                //timeData(t);
            }
            else {
                //timeData(t);
                Toast.makeText(getApplicationContext(), "Time" + spd.getLong("timer", 0), Toast.LENGTH_SHORT).show();
            }

        }

*/

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.timer:
                Toast.makeText(getApplicationContext(),"Timer",Toast.LENGTH_SHORT).show();
                    return true;

            default:
                return super.onOptionsItemSelected(item);
                /* LayoutInflater li = LayoutInflater.from(this);
                View promptsView = li.inflate(R.layout.dialog, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        this);

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView
                        .findViewById(R.id.input);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        // get user input and set it to result
                                        // edit text
                                        postData();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
*/
        }

}/*
    private void logToDisplay(final String line) {
        runOnUiThread(new Runnable() {
            public void run() {
                EditText editText = (EditText)RangingActivity.this.findViewById(R.id.rangingText);
                editText.append(line+"\n");
            }
        });
  */
    }


