package org.altbeacon.beaconreference;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;


// import static android.support.v4.content.ContextCompat.startActivity;
// import static com.cyronics.com.anzen.Splashscreen.MyPREFERENCES;


public class Logincredentials {
    Context mContext;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "User_Credentials" ;
    public static final String Password = "password";
    public static final String Phone = "phone";
    public static final String status = "login_status";
    public static final String register_token = "register_token";


    /** Instantiate the interface and set the context */
    Logincredentials(Context c) {
        mContext = c;
    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public void showToast(String phone,String password) {
        Toast.makeText(mContext,  "Saving Login Data ", Toast.LENGTH_SHORT).show();
        //Intent myIntent = new Intent(mContext, ScanActivity.class);
        //mContext.startActivity(myIntent);
        sharedpreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Phone, phone);
        editor.putString(Password, password);
        editor.commit();


        // method to check if login credentials are correct
        String add_token = sharedpreferences.getString(register_token, "");
        String verify_phone = phone ;
        String verify_password = password ;

        verify_add_token(add_token,verify_phone,verify_password);



    }

    // Post Request For JSONObject
    public void verify_add_token(String token,String verify_phone , String verify_password) {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        JSONObject object = new JSONObject();
        try {
            //input your API parameters
            object.put("token" , token  );
            object.put("verify_phone", verify_phone );
            object.put("verify_password", verify_password );

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Enter the correct url for your api service site
        String url = "http://34.123.218.195"+ "/register_token" ;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String output = "";
                        try {
                            output = response.get("response").toString();
                            if (output.matches("OK")){

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //resultTextView.setText("String Response : "+ output)
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), "Error getting response", Toast.LENGTH_SHORT).show();

            }
        });
        requestQueue.add(jsonObjectRequest);
    }

}
