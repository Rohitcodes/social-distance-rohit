package org.altbeacon.beaconreference;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.altbeacon.beacon.Beacon;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AdapterBeacon extends ArrayAdapter<Beacon> {
    private Context context;
    static String Txpower1;
    static int Txpower;
    static String ID1,name1,distance1,power2;
    static ArrayList<Beacon> allBeacons;
    private LayoutInflater mInflater;
    private boolean mNotifyOnChange = true;

    public AdapterBeacon(Context context, ArrayList<Beacon> mBeacons) {
        super(context,R.layout.row_beacon);
        this.context = context;
        this.allBeacons = new ArrayList<Beacon>(mBeacons);
        this.mInflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return allBeacons .size();
    }
    @Override
    public Beacon getItem(int position) {
        return allBeacons .get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public int getPosition(Beacon item) {
        return allBeacons .indexOf(item);
    }

    @Override
    public int getViewTypeCount() {
        return 1; //Number of types + 1 !!!!!!!!
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }


    public void copyBeacons(Collection<Beacon> beacons)
    {
        allBeacons.clear();
        for (Beacon b : beacons)
        {
            if (!allBeacons.contains(b)) {
                allBeacons.add(b);
            }
        }
        notifyDataSetChanged();
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
         int type = getItemViewType(position);
        if (convertView == null) {
            holder = new ViewHolder();
            switch (type) {
                case 1:
                    convertView = mInflater.inflate(R.layout.row_beacon,parent, false);
                    holder.nmspce=(TextView)convertView.findViewById(R.id.TVnm);
                    holder.name = (TextView) convertView.findViewById(R.id.nm);
                    holder.instance=(TextView)convertView.findViewById(R.id.TVid);
                    holder.ID=(TextView) convertView.findViewById(R.id.id1);
                    holder.TvDist=(TextView)convertView.findViewById(R.id.TvDist);
                    holder.TvTx1=(TextView)convertView.findViewById(R.id.TVtx);
                    holder.TxValue=(TextView)convertView.findViewById(R.id.TxValu);
                    holder.distance = (TextView) convertView.findViewById(R.id.dist);
                    holder.TvRssi=(TextView)convertView.findViewById(R.id.TVRssi);
                    holder.Dbm2=(TextView)convertView.findViewById(R.id.dBm1);
                    holder.power=(TextView) convertView.findViewById(R.id.power1);
                    break;
            }
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        name1=allBeacons.get(position).getId1().toString();
        ID1=allBeacons.get(position).getId2().toString();
        float distance= (float) allBeacons.get(position).getDistance();
        int power1=(int)allBeacons.get(position).getRssi();
        Txpower=(int)allBeacons.get(position).getTxPower();
        Txpower1=String.valueOf(Txpower);
        distance1=String.valueOf(distance);
        power2=String.valueOf(power1);
        holder.nmspce.setText("Beacons");
        holder.name.setText("Beacon-"+name1.substring(19));
        holder.instance.setText("Instance");
        holder.ID.setText(ID1);
        holder.TvTx1.setText("Tx");
        holder.TxValue.setText(""+Txpower1);
        holder.TvDist.setText("Distance");
        holder.distance.setText(distance1+"m");
        holder.TvRssi.setText("Rssi");
        holder.Dbm2.setText("dBm");
        holder.power.setText(power2);
        holder.pos = position;  /*      String Data="{"+"\"Name\""+"\""+ID1+"\","+"\","+"\"ID\""+"\""+name1+"\""+"}";
       Submit(Data);*/
            return convertView;
     }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        mNotifyOnChange = true;
    }

    public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }


    //---------------static views for each row-----------//
    static class ViewHolder {
        TextView nmspce;
        TextView name;
        TextView instance;
        TextView ID;
        TextView TvDist;
        TextView TvTx1;
        TextView TxValue;
        TextView distance;
        TextView TvRssi;
        TextView Dbm2;
        TextView power;
        int pos; //to store the position of the item within the list


    }

/*
    private void Submit(String data) {
        final String savedata=data;
        String URL="https://192/168.1.152/server.php";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject objres=new JSONObject(response);
                    Toast.makeText(getApplicationContext(),objres.toString(),Toast.LENGTH_LONG).show();


                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(),"Server Error",Toast.LENGTH_LONG).show();

                }
                //Log.i("VOLLEY", response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();

                //Log.v("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return savedata == null ? null : savedata.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    //Log.v("Unsupported Encoding while trying to get the bytes", data);
                    return null;
                }
            }

        };
        requestQueue.add(stringRequest);
    }






*/


}






