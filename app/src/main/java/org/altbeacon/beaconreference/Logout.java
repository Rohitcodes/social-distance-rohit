package org.altbeacon.beaconreference;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

//import static android.support.v4.content.ContextCompat.startActivity;
//import static com.cyronics.com.anzen.Splashscreen.MyPREFERENCES;


public class Logout {
    Context mContext;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "User_Credentials" ;
    public static final String Password = "password";
    public static final String Phone = "phone";
    public static final String status = "login_status";

    /** Instantiate the interface and set the context */
    Logout(Context c) {
        mContext = c;
    }

    /** Show a toast from the web page */
    @JavascriptInterface
    public void showToast() {
        Toast.makeText(mContext,  "Logging Out", Toast.LENGTH_SHORT).show();
        //Intent myIntent = new Intent(mContext, ScanActivity.class);
        //mContext.startActivity(myIntent);
        sharedpreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove(Phone);
        editor.remove(Password);
        editor.commit();

        Intent myIntent = new Intent(mContext, MonitoringActivity.class);
        myIntent.putExtra("url", mContext.getResources().getString(R.string.mainurl) + "/createaccount");

        mContext.startActivity(myIntent);
        ((Activity)mContext).finish();
       //getActivity().finish(); // Destroy activity A and not exist in Back stack

    }

}
