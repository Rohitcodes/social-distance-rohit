package org.altbeacon.beaconreference;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

public class NoInternet extends Activity {
    ImageView iv;
    SwipeRefreshLayout swipeRefreshLayout1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internet);
        iv=(ImageView)findViewById(R.id.idful);
        swipeRefreshLayout1=(SwipeRefreshLayout)findViewById(R.id.swipeit);
        swipeRefreshLayout1.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout1.setRefreshing(true);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout1.setRefreshing(false);
                        if(DetectConnection.checkInternetConnection(NoInternet.this))
                        {
                            Intent i=new Intent(NoInternet.this,MonitoringActivity.class);
                            startActivity(i);
                        }
                    }
                }, 1000);
            }
        });

        swipeRefreshLayout1.setColorSchemeColors(
                getResources().getColor(android.R.color.holo_blue_light),
                getResources().getColor(android.R.color.holo_orange_dark),
                getResources().getColor(android.R.color.holo_green_dark),
                getResources().getColor(android.R.color.holo_red_dark)
        );

    }
}